var quiz = [{
  "question": "Сколько будет 2+2",
  "choices": ["3", "1", "4"],
  "correct": "4"
}, {
  "question": "Корень из четырех?",
  "choices": ["2", "1", "5"],
  "correct": "2"
}, {
  "question": "10*10 = ?",
  "choices": ["100", "10", "1000"],
  "correct": "100"
}];


//основные элементы див блока вопросов
var content = $("content"),
  questionContainer = $("question"),
  choicesContainer = $("choices"),
  scoreContainer = $("score"),
  submitBtn = $("submit");

// переменные для цикла
var currentQuestion = 0,
  score = 0,
  askingQuestion = true;

function $(id) { 
  return document.getElementById(id);
}

function askQuestion() {
  var choices = quiz[currentQuestion].choices,
    choicesHtml = "";

  //цикл для формирования вариантов ответа
  for (var i = 0; i < choices.length; i++) {
    choicesHtml += "<input type='radio' name='quiz" + currentQuestion +
      "' id='choice" + (i + 1) +
      "' value='" + choices[i] + "'>" +
      " <label for='choice" + (i + 1) + "'>" + choices[i] + "</label><br>";
  }

  // загрузка вопроса
  questionContainer.textContent = "в" + (currentQuestion + 1) + ". " +
  quiz[currentQuestion].question;

  // загрузка панели выбора
  choicesContainer.innerHTML = choicesHtml;

  // первая загрузка
  if (currentQuestion === 0) {
    scoreContainer.textContent = "Очки: 0 из " +
      quiz.length + " возможных.";
    submitBtn.textContent = "Ответить";
  }
}

function checkAnswer() {
  // Следующий вопрос?
  if (askingQuestion) {
    submitBtn.textContent = "Следующий вопрос";
    askingQuestion = false;

    // Проверка вариантов
    var userpick,
      correctIndex,
      radios = document.getElementsByName("quiz" + currentQuestion);
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].checked) { 
        userpick = radios[i].value;
      }

      // индекс верного ответа
      if (radios[i].value == quiz[currentQuestion].correct) {
        correctIndex = i;
      }
    }

    
    var labelStyle = document.getElementsByTagName("label")[correctIndex].style;
    labelStyle.fontWeight = "bold";
    if (userpick == quiz[currentQuestion].correct) {
      score++;
      labelStyle.color = "green";
    } else {
      labelStyle.color = "red";
    }

    scoreContainer.textContent = "Очки: " + score + " из " +
      quiz.length + " возможных.";
  } else { 
    askingQuestion = true;
    // Меняем текст кнопки на ответить
    submitBtn.textContent = "Ответить";
    // Последний вопрос?
    if (currentQuestion < quiz.length - 1) {
      currentQuestion++;
      askQuestion();
    } else {
      showFinalResults();
    }
  }
}

function showFinalResults() {
  content.innerHTML = "<h2>Поздравляем вы завершили тест!</h2>" +
    "<h2>Ваш результат:</h2>" +
    "<h2>" + score + " из " + quiz.length + " вопросов, " +
    Math.round(score / quiz.length * 100) + "%<h2>";
}

window.addEventListener("load", askQuestion, false);
submitBtn.addEventListener("click", checkAnswer, false);
